import pika
import time
import logging


def connect(rabbitmq_url, queue):
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_url))
        channel = connection.channel()
        channel.queue_declare(queue=queue)
        return True, connection, channel
    except Exception as ex:
        return False, None, None


def Setup(rabbitmq_url, queue):
    try:
        i = 0
        success, connection, channel = connect(rabbitmq_url, queue)
        while i < 5 and not success:
            i += 1
            logging.error(
                "[BROKER] ERROR : Cannot connect to broker(s) retry "
                + str(i)
                + " in 5 seconds . . ."
            )
            time.sleep(5)
            success, connection, channel = connect(rabbitmq_url, queue)
        if not success:
            logging.fatal(
                "[BROKER] FATAL : No broker available after " + str(i) + " try. Stop"
            )
            exit(code=-1)
        logging.info("[BROKER]: Broker connected")
        return channel, connection
    except Exception as ex:
        logging.error("[BROKER] ERROR : Broker setup : ", type(ex), ex)


def send(channel, routing_key, message):
    logging.info(
        "[x] Sending MQTT message : queue <" + routing_key + "> data " + str(message)
    )
    channel.basic_publish(exchange="", routing_key=routing_key, body=message)
