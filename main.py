import os
import pika
import time
import random
import threading
import logging

import Broker
import PickProcess

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter

if __name__ == "__main__":
    host = (
        "http://" + os.getenv("API_HOST", "api") + ":" + os.getenv("API_PORT", "8000")
    )
    client = new_client(RequestsAdapter(), Configuration(host=host))
    RABBIT_MQ_URL = os.getenv("RABBIT_MQ_URL", "broker")
    QUEUE_WRITE_MAPPER = "/ptl/mapper/main"
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        level=logging.INFO,
        datefmt="[%d/%m/%Y %H:%M:%S]",
    )

    channel, connection = Broker.Setup(RABBIT_MQ_URL, QUEUE_WRITE_MAPPER)

    while True:
        try:
            setting = client.settings.settings_retrieve("MODE")
            second = random.randint(2, 15)
            if setting.value == "DEMO":
                if channel.is_closed or connection.is_closed:
                    channel, connection = Broker.Setup(
                        RABBIT_MQ_URL, QUEUE_WRITE_MAPPER
                    )
                x = threading.Thread(
                    target=PickProcess.picking,
                    args=(channel, QUEUE_WRITE_MAPPER, client),
                )
                x.start()
                logging.info("[MAIN] Waiting " + str(second) + "sec...")
            else:
                logging.info(
                    "[MAIN] Application in <"
                    + str(setting.value)
                    + "> mode. No simulation"
                )
            time.sleep(second)
        except KeyboardInterrupt as ex:
            logging.info("[MAIN] STOP process due to keyboard interrupt")
            break
        except Exception as ex:
            logging.exception(ex, exc_info=True)
    connection.close()
    logging.info("[MAIN] Bye")
