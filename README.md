# Simulator Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Python](https://www.python.org/).

Le rôle de ce simulator est de simuler le comportement d'opérateurs sur la ligne de production.

! Attention les gammes ne sont pas pris en compte, la simulation se limite juste à allumer des LEDs de manière aléatoire sur la ligne.
L'extinction quand à elle vient entre 5 à 15 seconde après l'allumage.

# Documentation


`Broker.py` contient les éléments de connexion et reconnexion au message broker.

`main.py` est le point d'entrée du programme, connexion, la boucle d'allumage...

`PickProcess.py` lorsqu'il est lancé par la boucle d'allumage va choisir au hasard un bac sur la ligne de production et lancer envoyer un message MQTT au mapper.
Les informations envoyés au mapper sont identiques aux informations envoyé par le providers (poste, article, status) afin de se rapprocher au maximum de la réalité.
Une fois le message allumé le processus va attendre entre 5 et 15 sec pour éteindre les lumières.

Remarque : Plusieurs process de Picking peuvent fonctionner en même temps. Cela peut-être un peu perturbant à la lecture des logs

# Variable d'environement

- RABBIT_MQ_URL: (default: broker)
- API_HOST: (default api)
- API_PORT: (default 8000)

# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)
