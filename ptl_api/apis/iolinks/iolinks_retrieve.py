from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class IoLink(BaseModel):
    id: str 
    ip: str 

def make_request(self: BaseApi,


    id: str,

) -> IoLink:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/iolinks/{id}/".format(
            
                id=id,
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": IoLink,
            
        },
    
    }, m)