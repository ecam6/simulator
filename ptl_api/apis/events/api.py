from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import events_list
from . import events_create
from . import events_retrieve
from . import events_update
from . import events_partial_update
from . import events_destroy
class EventsApi(BaseApi):
    events_list = events_list.make_request
    events_create = events_create.make_request
    events_retrieve = events_retrieve.make_request
    events_update = events_update.make_request
    events_partial_update = events_partial_update.make_request
    events_destroy = events_destroy.make_request