import Broker
import logging
import time
import random
import os
import json


def get_random_item(items):
    nb_item = len(items)
    if nb_item == 0:
        return None
    return items[random.randint(0, nb_item - 1)]


def find_box_in_workstations(workstations, box_id):
    for w in workstations.results:
        for l in w.lines:
            for b in l.boxes:
                if b.id == box_id:
                    return w, l, b


def get_data(state, workstation, article):
    return json.dumps(
        {"state": state, "workstation": workstation.pos, "article": article.id_ext}
    )


def picking(channel, routing_key, client):
    boxes = client.boxes.boxes_list()
    box = get_random_item([a for a in boxes.results if not a.is_on])
    if box == None:
        logging.info("[PICK] All boxes are on... Do nothing")
        return
    workstation, line, box = find_box_in_workstations(
        client.workstations.workstations_list(), box.id
    )

    article = client.articles.articles_retrieve(box.article)
    Broker.send(channel, routing_key, get_data(True, workstation, article))
    second = random.randint(5, 15)
    logging.info("[PICK] Waiting " + str(second) + "... for article to be picked")
    time.sleep(second)
    Broker.send(channel, routing_key, get_data(False, workstation, article))
